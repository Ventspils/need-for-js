'use strict';

const score = document.querySelector('.score'),
  start = document.querySelector('.start'),
  gameArea = document.querySelector('.gameArea'),
  car = document.createElement('div');

const keys = {
  ArrowUp: false,
  ArrowDown: false,
  ArrowRigh: false,
  ArrowLeft: false,
};

const state = {
  started: false,
  score: 0,
  speed: 3,
};

const playGame = function () {
  console.log('Play game!');
  if (state.started) requestAnimationFrame(playGame);
};

const startGame = function () {
  start.classList.add('hide');
  gameArea.appendChild(car);
  car.classList.add('car');
  state.started = true;
  requestAnimationFrame(playGame);
};

const startRun = function (event) {
  event.preventDefault();
  keys[event.key] = true;
};

const stopRun = function (event) {
  event.preventDefault();
  keys[event.key] = false;
};

start.addEventListener('click', startGame);
document.addEventListener('keydown', startRun);
document.addEventListener('keyup', stopRun);
